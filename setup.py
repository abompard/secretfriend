#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name="secretfriend",
    version="0.2",
    description="Organize secret friend events",
    #long_description=open('README.rst').read(),
    author='Aurelien Bompard',
    author_email='aurelien@bompard.org',
    url="https://gitlab.com/abompard/secretfriend",
    license="AGPLv3+",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Programming Language :: Python :: 3",
        ],
    packages=find_packages(),
    #include_package_data=True,
    install_requires=[
        "Flask-SQLAlchemy",
        "Flask",
        ],
    tests_requires=["nose2"],
    test_suite='nose2.collector.collector',
    entry_points={
        'console_scripts': [
            'secretfriend = secretfriend.command:main',
            ],
        },
    )
