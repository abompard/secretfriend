import os
import sys
import argparse

from sqlalchemy.orm.exc import NoResultFound
from secretfriend import app, db, Event, Friend


def get_event(title_or_id):
    try:
        event = int(title_or_id)
    except ValueError:
        event = title_or_id
        try:
            return Event.query.filter_by(title=event).one()
        except NoResultFound:
            return None
    else:
        return Event.query.get(event)


class Command:
    """Base command class"""

    name = None
    _help_text = None
    _parser = None
    _arguments = []

    def make_parser(self, subparser):
        self._parser = subparser.add_parser(self.name, help=self._help_text)
        self._parser.set_defaults(func=self.run)
        for arg in self._arguments:
            self._parser.add_argument(arg)

    def run(self, args):
        raise NotImplementedError


class SyncDB(Command):

    name = "syncdb"

    def run(self, args):
        db.create_all()
        db.session.commit()


class ManageEvents(Command):
    name = "events"

    def make_parser(self, subparser):
        #super(ManageEvent, self).make_parser(subparser)
        self._parser = subparser.add_parser(self.name, help=self._help_text)
        self._parser.set_defaults(func=lambda args: self._parser.print_help())
        action_parsers = self._parser.add_subparsers()
        list_parser = action_parsers.add_parser("list", help="List events")
        list_parser.set_defaults(func=self.list_all)
        add_parser = action_parsers.add_parser("add", help="Add an event")
        add_parser.add_argument("title")
        add_parser.set_defaults(func=self.add)
        del_parser = action_parsers.add_parser("del", help="Add an event")
        del_parser.add_argument("title_or_id")
        del_parser.set_defaults(func=self.delete)
        enable_parser = action_parsers.add_parser("enable", help="Activate an event")
        enable_parser.add_argument("title_or_id")
        enable_parser.set_defaults(func=self.enable)
        disable_parser = action_parsers.add_parser("disable", help="Deactivate an event")
        disable_parser.add_argument("title_or_id")
        disable_parser.set_defaults(func=self.disable)

    def add(self, args):
        title = args.title
        if Event.query.filter_by(title=title).count() > 0:
            print("Event \"%s\" already exists" % title)
        else:
            db.session.add(Event(title=title))
            db.session.commit()
            print("Event %s created." % title)

    def list_all(self, args):
        for event in Event.query.order_by(Event.title).all():
            status = "active" if event.active else "inactive"
            print("{event.id}\t{event.title}\t{status}".format(
                  event=event, status=status))

    def enable(self, args):
        event = get_event(args.title_or_id)
        if event is None:
            print("No such event: %s" % args.event, file=sys.stderr)
            return 1
        event.active = True
        db.session.commit()

    def disable(self, args):
        event = get_event(args.title_or_id)
        if event is None:
            print("No such event: %s" % args.event, file=sys.stderr)
            return 1
        event.active = False
        db.session.commit()

    def delete(self, args):
        event = get_event(args.title_or_id)
        if event is None:
            print("No such event: %s" % args.event, file=sys.stderr)
            return 1
        db.session.delete(event)
        db.session.commit()


class ManageFriends(Command):
    name = "friends"

    def make_parser(self, subparser):
        #super(ManageFriends, self).make_parser(subparser)
        self._parser = subparser.add_parser(self.name, help=self._help_text)
        self._parser.set_defaults(func=lambda args: self._parser.print_help())
        action_parsers = self._parser.add_subparsers()
        list_parser = action_parsers.add_parser("list", help="List friends")
        list_parser.add_argument("event")
        list_parser.set_defaults(func=self.list_all)
        add_parser = action_parsers.add_parser("add", help="Add a friend")
        add_parser.add_argument("event")
        add_parser.add_argument("name")
        add_parser.set_defaults(func=self.add)

    def add(self, args):
        event = get_event(args.event)
        if event is None:
            print("No such event: %s" % args.event, file=sys.stderr)
            return 1
        name = args.name
        if Friend.query.filter_by(event=event, name=name).count() > 0:
            print("Friend \"%s\" already exists for event \"%s\""
                  % (name, event.title))
        else:
            db.session.add(Friend(event=event, name=name))
            db.session.commit()
            print("Friend %s added." % name)

    def list_all(self, args):
        event = get_event(args.event)
        if event is None:
            print("No such event: %s" % args.event, file=sys.stderr)
            return 1
        friends = Friend.query.filter_by(event=event).order_by(Friend.name).all()
        for friend in friends:
            print(friend.name)


class Run(Command):
    name = "run"
    def run(self, args):
        app.run(host="0.0.0.0", debug=True)



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="Load this configuration file")
    subparsers = parser.add_subparsers(help='sub-command help')
    for action_class in Command.__subclasses__():
        action_class().make_parser(subparsers)
    args = parser.parse_args()
    if args.config is not None:
        args.config = os.path.abspath(args.config)
        if os.path.exists(args.config):
            app.config.from_pyfile(args.config)
        else:
            parser.error("can't find the config file: %s" % args.config)
    sys.exit(args.func(args))


if __name__ == '__main__':
    main()
