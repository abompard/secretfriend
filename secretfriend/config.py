import os.path
project_dir = os.path.abspath(
    os.path.join(os.path.dirname(__file__), ".."))

SQLALCHEMY_DATABASE_URI = \
    'sqlite:///' + os.path.join(project_dir, "secretfriend.db")

# You can ignore values below this line
SQLALCHEMY_TRACK_MODIFICATIONS = False
