import os
from random import choice
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm.exc import NoResultFound

app = Flask(__name__)
app.config.from_object('secretfriend.config')
if 'FLASK_SETTINGS' in os.environ:
    app.config.from_envvar('FLASK_SETTINGS')

db = SQLAlchemy(app)


#
# DB
#

class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(255), nullable=False, unique=True)
    active = db.Column(db.Boolean, default=True)
    friends = db.relationship("Friend",
        backref="event", cascade="all, delete-orphan", passive_deletes=True)

    def __repr__(self):
        return '<Event %r>' % self.title

class Friend(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer,
        db.ForeignKey("event.id", ondelete="cascade", onupdate="cascade"),
        nullable=False)
    name = db.Column(db.Unicode(255), nullable=False)
    gives_to_id = db.Column(db.Integer,
        db.ForeignKey("friend.id", onupdate="cascade"), nullable=True)
    gives_to = db.relationship(
        "Friend", backref="given_by", uselist=False, remote_side=[id])

    def __repr__(self):
        return '<Friend %r>' % self.name


#
# Views
#
from flask import render_template, request, redirect, url_for

@app.route('/', methods=['GET', 'POST'])
def index():
    active_events = Event.query.filter_by(active=True).order_by(Event.title).all()
    if len(active_events) == 1:
        return redirect(url_for("event", event_id=active_events[0].id))
    return render_template("index.html", events=active_events)

@app.route('/e/<int:event_id>', methods=['GET', 'POST'])
def event(event_id):
    event = Event.query.get_or_404(event_id)
    undecided_friends = Friend.query.filter_by(
        event=event, gives_to_id=None).order_by(Friend.name).all()
    context = {
        "event": event,
        "friends": tuple(f.name for f in undecided_friends),
        }
    if request.method == 'POST':
        try:
            chosen_friend = choose_friend(request.form["giver"].strip())
        except ValueError as e:
            context["error"] = e
        else:
            context["chosen_friend"] = chosen_friend
    return render_template("event.html", **context)


#
# Lib
#

def choose_friend(giver_name):
    try:
        giver = Friend.query.filter_by(name=giver_name).one()
    except NoResultFound:
        raise ValueError("Unknown friend: {}".format(giver_name))
    if giver.gives_to_id is not None:
        raise ValueError("You have already been assigned a friend")
    available_friends = Friend.query.filter(db.and_(
            Friend.name != giver_name,
            db.not_(Friend.id.in_(
                db.session.query(Friend.gives_to_id).filter(Friend.gives_to_id != None)
            )))).all()
    chosen_friend = choice(available_friends)
    giver.gives_to = chosen_friend
    db.session.commit()
    return chosen_friend
